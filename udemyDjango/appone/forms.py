from django import forms
from appone.models import Song


class TestForm(forms.Form):

    CITIES = (
        (0, 'Paris'),
        (1, 'Miami'),
        (2, 'New York')
    )
    name = forms.CharField(label='Your name', max_length=50)
    email = forms.EmailField(label='Your email', max_length=100, required=False)
    yes_no = forms.BooleanField(label='Either Yes or No')
    city = forms.ChoiceField(label='Your city', choices=CITIES)


class PyBoxForm(forms.Form):

    PLAN = (
        (0, 'basic'),
        (1, 'premium'),
        (2, 'deluxe')
    )

    name = forms.CharField(label='Your name', max_length=50)
    email = forms.EmailField(label='Your email', max_length=100, required=False)
    signup_to_newsletter = forms.BooleanField(label='Either Yes or No')
    plan = forms.ChoiceField(label='Your plan', choices=PLAN)


class SongForm(forms.Form):
    class Meta:
        model = Song
        fields = ['name', 'duration']