
from django.contrib import admin
from django.urls import path

from apptwo import views as apptwo_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('djangorocks/', apptwo_views.djangorocks),
    #path('pictures/<str:category>/<int:year>/<int:month>/', apptwo_views.picture_detail),
    path('pictures/<str:nom>/<str:prenom>/', apptwo_views.picture_detail),
]
