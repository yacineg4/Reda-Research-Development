from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader


def djangorocks(request):
    return HttpResponse('This is a Jazzy Response')


def picture_detail(request, nom, prenom):
    # return HttpResponse("Category = {}, year = {}, month = {}".format(category, year, month))
    template_index = loader.get_template("apptwo/index.html")

    context = {
        'nom': nom,
        'prenom': prenom,
    }

    return HttpResponse(template_index.render(context, request))


