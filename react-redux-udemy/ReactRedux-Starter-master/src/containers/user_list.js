import React, { Component } from "react";
import { connect } from "react-redux";
import { selectUser } from "../actions/index";
import { bindAction } from "redux";

class UserList extends Component {
  render() {
    return (
      <div>
        <ul>
          {this.props.myUsers.map(user => {
            return (
              <li className="list-group-item" key={user.id}>
                {user.name}
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    myUsers: state.users
  };
}

function mapDispatchToProps(state) {
  bindActionCreators({ selectUser: selectUser }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList);
