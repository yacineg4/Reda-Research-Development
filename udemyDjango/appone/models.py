from django.db import models
from django.contrib.auth.models import User
from users.models import CustomUser


class Song(models.Model):
    album = models.ForeignKey('Album',
                              on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    duration = models.IntegerField(default=0, help_text="Duration in seconds")
    lyrics = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Album(models.Model):
    name = models.CharField(max_length=255)
    artist_name = models.CharField(max_length=255)
    release_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return '{} - {}'.format(self.name, self.artist_name)


class Device(models.Model):
    OS_ANDROID = 'android'
    OS_IOS = 'iOS'

    CHOICE_OS = (
        (OS_ANDROID, "Android"),
        (OS_IOS, "ios")
    )

    FORM_FACTOR_PHONE = 'phone'
    FORM_FACTOR_TABLET = 'tablet'

    CHOICE_FORM_FACTOR = (
        (FORM_FACTOR_PHONE, "Phone"),
        (FORM_FACTOR_TABLET, "Tablet")
    )

    os = models.CharField(max_length=20,
                          choices=CHOICE_OS,
                          default=OS_ANDROID)

    format_factor = models.CharField(max_length=20,
                                     choices=CHOICE_FORM_FACTOR,
                                     default=FORM_FACTOR_PHONE)

    model = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    description = models.TextField(blank=True)
    enabled = models.BooleanField(default=True)

    def __str__(self):
        return '{pk} : {os}, {format_factor}, {model}, {created_at}, {description}, {enabled}'.format(
            pk=self.pk,
            os=self.os,
            format_factor=self.format_factor,
            model=self.model,
            created_at=self.created_at,
            description=self.description,
            enabled=self.enabled
        )


class Company(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return 'id : {} , name : {}'.format(self.pk, self.name)


class Department(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return 'id : {} , name : {}'.format(self.pk, self.name)


class Employee(models.Model):
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    department = models.ForeignKey('Department', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    age = models.IntegerField()
    salary = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return 'id : {id}, company : {company}, department : {department}, name : {name}, age : {age},' \
               ' salary : {salary}'.format(
                id=self.pk,
                company=self.company,
                department=self.department,
                name=self.name,
                age=self.age,
                salary=self.salary
        )


class Name(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return 'id : {} , name : {}'.format(self.pk, self.name)


class Profile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    def __str__(self):
        return f'{self.user.username} Profile'


class Compagnie(models.Model):
    companyname = models.CharField(max_length=100)
    services = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    website = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.companyname}'