from django.contrib import admin

from appone.models import Album, Song, Name, Profile, Compagnie


class AlbumAdmin(admin.ModelAdmin):
    readonly_fields = ('release_date', )


class SongAdmin(admin.ModelAdmin):
    readonly_fields = ('lyrics', )


admin.site.register(Album, AlbumAdmin)
admin.site.register(Song, SongAdmin)
admin.site.register(Name)
admin.site.register(Profile)
admin.site.register(Compagnie)



