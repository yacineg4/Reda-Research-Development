from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from appone.models import Song, Name, Profile, Compagnie
import uuid
import boto3

from appone.models import Device
from appone.forms import TestForm
from appone.forms import PyBoxForm
from appone.forms import SongForm
from rest_framework.decorators import api_view



def hello(request):
    return HttpResponse('Hello Django! appone application')


def song_list(request):
    names = []
    for s in Song.objects.all():
        names.append(s.name+" ")

    return HttpResponse(names)


def song_add(request, name, duration, lyrics, album_id):
    song = Song(album_id=album_id, name=name, duration=duration, lyrics=lyrics)
    song.save()
    return HttpResponse("song {} with id : {} saved".format(name, song.id))


def device_list(request, id):
    devices_name = []
    for d in Device.objects.filter(id=id):
        devices_name.append(d)
    return HttpResponse(devices_name)


def device_list_all(request):
    devices_name = []
    for d in Device.objects.all():
        devices_name.append(d)
    return HttpResponse(devices_name)


def device_add(request, os, model):
    device = Device(os=os, model=model)
    device.save()
    return HttpResponse("device {} created".format(device))


def get_form_data(request):
    form = TestForm(request.POST)

    if form.is_valid():
        print('In POST processing')
        print(form.cleaned_data['name'])
        print(form.cleaned_data['city'])
        print(form.cleaned_data['yes_no'])
        print(form.cleaned_data['email'])

    form = TestForm()
    return render(request, 'appone/form.html', {'form': form})


def get_form_pybox_data(request):
    form = PyBoxForm(request.POST)

    if form.is_valid():
        print('In POST processing')
        print(form.cleaned_data['name'])
        print(form.cleaned_data['plan'])
        print(form.cleaned_data['signup_to_newsletter'])
        print(form.cleaned_data['email'])

    form = PyBoxForm()
    return render(request, 'appone/form_pybox.html', {'form_pybox': form})


def song_create(request):
    form = SongForm()
    return render(request, 'appone/song_create.html', {'form_song': form})


@api_view(['POST'])
def get_nom(request):
    print(request.data)
    if request.method == 'POST':
        name = request.POST.getlist('name')
        var = Name(name=name,)
        var.save()
        return HttpResponse(name)


@api_view(['POST'])
def get_compagnie(request):
    if request.method == 'POST':
        companyname = request.POST.getlist('companyname')
        services = request.POST.getlist('services')
        email = request.POST.getlist('email')
        phone = request.POST.getlist('phone')
        address = request.POST.getlist('address')
        website = request.POST.getlist('website')
        var = Compagnie(companyname=companyname, services=services, email=email, phone=phone, address=address, website=website)
        var.save()
        return HttpResponse(companyname)