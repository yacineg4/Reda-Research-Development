"""udemyDjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework_jwt.views import obtain_jwt_token

from appone import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello/', views.hello),
    path('songs/', views.song_list),
    path('songs/<int:album_id>/<str:name>/<int:duration>/<str:lyrics>/', views.song_add),
    path('devices/add/<str:os>/<str:model>/', views.device_add),
    path('devices/<int:id>/', views.device_list),
    path('devices/all/', views.device_list_all),
    path('apptwo/', include('apptwo.urls')),

    path('forms/', views.get_form_data, name='get_form_data'),
    # Dans le html, si l'utilisateur met comme name = 'url get_form_data', les données seront traité
    # dans views.get_form_data au click de submit
    path('register/', views.get_form_pybox_data, name='get_form_pybox_data'),
    path('thanks/', views.get_form_pybox_data, name='get_form_pybox_data'),
    path('songs/create/', views.song_create),
    path('name/', views.get_nom),
    #path('photo/add/',views.add_photo, name='add_photo'),

    path('token-auth/', obtain_jwt_token),
    path('core/', include('core.urls')),
    path('compagnie/', views.get_compagnie)

]

